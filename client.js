/* create a client class: get all users using the APIs & then remove the age
 attribute and add a "year" attribute that the value of the year the age represents
 for example if the age was 55 thent the year is 2018-55. Later put this info user
 to a new file called "updated_data".
 everytime a user gets a an insertion success in the new file, delete the user from the 
 original file using the APIs. If the user is not inserted, you don't delete the user and you 
  log an error
  */
var http = require('http');
const fs = require('fs')

function getPeopleAndModify(callback) {
    return http.get({
        host: 'localhost',
        port: '8080',
        path: '/api/people',
        headers: {
            'secret': 'bootcamp'
        }
    }, function (response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', (data) => {
            body += data;
        });
        response.on('end', () => {
            // Data reception is done, callback with parsed JSON!
            var parsed = JSON.parse(body);
            callback(parsed);
        });
        response.on('error', () => {
            console.log(error)
        })
    });

}
//Delete people from old data.json
function deletePeopleOld(id) {
    return http.request({
        host: 'localhost',
        port: '8080',
        path: "/api/people/" + id,
        method: "delete",
        headers: {
            'secret': 'boo2tcamp'
        }
    }, (response) => {
    }).end();
}
//for testing purposes...must be moved to new file
getPeopleAndModify((data) => {
    let chunk = { people: [], secret: "bootcamp" }
    data.parsed.forEach(element => {
        chunk.people.push({ id: element.id, name: element.name, year: new Date().getFullYear() - element.age });
        deletePeopleOld(element.id);
    });
    var stringified = JSON.stringify(chunk, null, "\t");
    fs.writeFileSync('updated_data.json', stringified);
});