//requires
const fs = require('fs')
let rawdata = fs.readFileSync('data.json')
let data = JSON.parse(rawdata)

//check secret from json file
let checkSecret = (secret) => {
    return secret === data.secret ? false : true;

}
//check secret and delete accordingly
exports.remove = (id, secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    for (var key in data.people) {
        if (data.people[key].id === id) {
            data.people.splice(key, 1);
        }
    }
    var stringified = JSON.stringify(data, null, "\t");
    fs.writeFileSync('data.json', stringified);
}