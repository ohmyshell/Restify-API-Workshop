//requires
const fs = require('fs')
let rawdata = fs.readFileSync('data.json')
let data = JSON.parse(rawdata)

//check secret from json file
let checkSecret = (secret) => {
    return secret === data.secret ? false : true;

}
//check secret then return all people
exports.getAll = (secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    return data.people;
}
// check secret then return a matching id if it exists
exports.getByID = (id, secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    for (var key in data.people) {
        if (data.people[key].id === id) {
            return data.people[key];
        }
    }
}

exports.getIDs = (secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    let ids = [];
    for (var key in data.people) {
        ids.push(data.people[key].id);
    }
    return ids;
}