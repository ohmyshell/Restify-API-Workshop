//requires file system
const fs = require('fs')
let rawdata = fs.readFileSync('data.json')
let data = JSON.parse(rawdata)

//check secret from json file
let checkSecret = (secret) => {
    return secret === data.secret ? false : true;

}

//update certain person but check secret first
exports.updating = (id, name, age, secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    for (var key in data.people) {
        console.log(data.people[key].id)
        if (data.people[key].id == id) {
            if (name) {
                data.people[key].name = name;
            }
            if (age) {
                data.people[key].age = age;
            }
        }
        var stringified = JSON.stringify(data, null, "\t");
        fs.writeFileSync('data.json', stringified);
    }
}
// this was a call for testing
// this.updating(9, "Pardons", 464, "bootcamp");