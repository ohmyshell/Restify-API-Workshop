// this class is no longer used but is kept for later
// it was made to have all the functiionalities of 
// POST, GET, DEL
const fs = require('fs')
let rawdata = fs.readFileSync('data.json')
let data = JSON.parse(rawdata)

//check secret from json file
let checkSecret = (secret) => {
    return secret === data.secret ? false : true;

}
//check secret and add according to result
exports.add = (userId, userName, userAge, secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    let user = { id: parseInt(userId), name: userName, age: parseInt(userAge) };
   // checking if we already have that ID, if we don't have it we push 
    if ((JSON.stringify(data.people).indexOf("\"id\":" + user.id)) < 0) {
        data.people.push(user);
    }
    // the 2 optional parameters are for formatting the JSON file
    var stringified = JSON.stringify(data, null, "\t");
    fs.writeFileSync('data.json', stringified);

}
//check secret and delete accordingly
exports.delete = (id, secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    for (var key in data.people) {
        if (data.people[key].id === id) {
            data.people.splice(key, 1);
        }
    }
    var stringified = JSON.stringify(data, null, "\t");
    fs.writeFileSync('data.json', stringified);
}

//check secret then return all people
exports.get = (secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    return data.people;
}

exports.getByID = (id,secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    for (var key in data.people) {
        if (data.people[key].id === id) {
            return data.people[key];
        }
    }
}
//update certain person but check secret first
exports.update = (id, name, age, secret) => {
    if (checkSecret(secret))
        return "Secret Incorrect"
    for (var key in data.people) {
        if (data.people[key].id === id) {
            if (name) {
                data.people[key].name = name;
            }
            if (age) {
                data.people[key].age = age;
            }
        }
        var stringified = JSON.stringify(data, null, "\t");
        fs.writeFileSync('data.json', stringified);
    }
}