# In order to get the server functional some dependencies need to be installed.

## Important
```bash 
npm install restify
```

### Recommended
```bash
npm install restify-router
npm install restify-error
```